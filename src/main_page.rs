use iced::{pure::{
    widget::{Container, Text},
    Element,
}, Length};
use crate::CounterMessage;

pub struct MainPage;

impl MainPage {
    pub fn new() -> MainPage {
        MainPage
    }

    pub fn view(&self) -> Element<CounterMessage> {
        Container::new(Text::new("hello from page 2"))
            .width(Length::Fill)
           .height(Length::Fill)
            .center_x()
           .center_y()
            .into()
    }
}
#[derive(Debug,Clone, Copy)]
pub enum Views {
    Counter,
    Main
}